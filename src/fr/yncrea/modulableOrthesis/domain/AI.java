package fr.yncrea.modulableOrthesis.domain;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;

public class AI extends JFrame implements KeyListener {
	
	//KEYBOARD TMP
	private char key;
	
	//AI
	private Neuron[] neurons;
	private int nbNeuron;
	private File weightFile;
	
	//STIFNESS
	private Random rand;
	private final float MIN_TENSION_VALUE 		= 0.f;
	private final float MAX_TENSION_VALUE 		= 2.f;
	private final int   NB_LOOP_LEARNING 		= 500000000; //AI nb loop for neuron's learning
	
	//TOOLS
	private Tools tools;
	
	
	
	public AI(int nbNeuron, int nbEntries) {
		this.key = 'n';
		this.rand = new Random();
		this.nbNeuron = nbNeuron;
		this.tools = new Tools();
		this.weightFile = new File("weight_save.txt");
		
		//1) Init neuron
		this.neurons = new Neuron[nbNeuron];
		for(int n=0; n < nbNeuron; n++) {
			this.neurons[n] = new Neuron(nbEntries);
		}
		
		//2) Create default windows for keyboard detection
		addKeyListener(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	
	
	//Main loop
	public void launch() {
		
		while(this.key != 'x') {
			//MAIN MENU
			tools.menuDisplay();
			while(this.key != 's' && this.key != 'x' && this.key != 'd') {
				System.out.print("");
			}
			
			
			//CALIBRATION
			if(this.key == 's') {
				System.out.println("\n/////////////// CALIBRATION ///////////////");
				
				//1) Learn until NB_LOOP_LEARNING is reached
				List<Float> weightsGot = new ArrayList<Float>(); // ID 0 : Weight | ID 1 : Bias
				this.calibration(weightsGot);
				
				//2) Save new weight
				tools.saveWeight(weightFile, weightsGot.get(0), weightsGot.get(1));
			}
			
			
			//DECISION
			if(this.key == 'd') {
				System.out.println("\n/////////////// DECISION ///////////////");
				
				//1) Get weight from file
				List<Float> weightsGot = new ArrayList<Float>();
				weightsGot = tools.getWeight(weightFile);
				
				//2) Set neuron's values
				for(int numNeuron=0; numNeuron<this.nbNeuron; numNeuron++) { 			  //For each neuron
					this.neurons[numNeuron].synapses[0] = weightsGot.get(0).floatValue(); //Weight
					this.neurons[numNeuron].synapses[1] = weightsGot.get(1).floatValue(); //Bias
				}
				System.out.println("AI -> WEIGHT GOT : " + this.neurons[0].synapses[0] + " \tBIAS GOT : " + this.neurons[0].synapses[1]);
				
				//3) Launch AI decision
				long nbError = 0;
				long nbLoop = 0;
				while(this.key != 'x') {
					nbError += this.decision();
					
					nbLoop++;
					System.out.println(" \tNB ERRORS : " + nbError + "   \tNB LOOP : " + nbLoop);
				}
				
				
				//INFO//
				DecimalFormat formatter = new DecimalFormat("0.000");
				String ratio = formatter.format((((float)nbError*100)/(float)nbLoop)) ;
				System.out.println("\n\nRESULTS -> RATIO (E/L) : " + ratio + " % \t (" + nbError + " ERRORS IN " + nbLoop + " LOOPS)");
			}
		}
		
		//EXIT
		System.out.println("\n//////////////////// EXIT ////////////////////");
		System.exit(0);
	}
	
	
	
	//Let AI decide if tension = stiffness
	private int decision() {
		
		//1) TMP : Generate random stiffness value
		float min = this.key == ' ' ? MAX_TENSION_VALUE/2 : MIN_TENSION_VALUE;
		float max = this.key == ' ' ? MAX_TENSION_VALUE   : MAX_TENSION_VALUE/2;
		float realTimeStiffnessValue = min + (max - min) * rand.nextFloat();
		
		//2) Let AI decide
		for(int numNeuron=0; numNeuron<this.nbNeuron; numNeuron++) { //For each neuron
			this.neurons[numNeuron].update(realTimeStiffnessValue);
			if(this.neurons[numNeuron].output > 0.0) {
				System.out.print("AI -> SFN : YES  \tVALUE : " + realTimeStiffnessValue);
				
				if(realTimeStiffnessValue < 1.0f) {
					return 1;
				}
			}
			else {
				System.out.print("AI -> SFN : NO  \tVALUE : " + realTimeStiffnessValue);
				
				if(realTimeStiffnessValue > 1.0f) {
					return 1;
				}
			}
		}
		
		return 0;
	}
	
	
	
	//Calibrate AI according to value wanted
	private void calibration(List<Float> weightsGot) {
				
		//1) Loop until nb wanted iterations
		List<Float> weights = new ArrayList<Float>();
		List<Float> biases = new ArrayList<Float>();
		for(long nbLoop=0; nbLoop<NB_LOOP_LEARNING; nbLoop++) {
			
			//1.1) Generate random normal stiffness value (0~1)
			float currentStiffnessValue = MIN_TENSION_VALUE + (MAX_TENSION_VALUE - MIN_TENSION_VALUE) * rand.nextFloat();
			
			//1.2) Define if is stiffness tension value
			float resultWanted = currentStiffnessValue > 1.0f ? 1.0f : 0.0f;
			
			//1.3) Learn each new input
			this.learning(currentStiffnessValue, resultWanted);
			
			//1.4) Save new result
			//weights.add(neurons[0].synapses[0]); //WEIGHT
			//biases.add(neurons[0].synapses[1]);	 //BIAS
			
			System.out.println((long)((nbLoop*100)/NB_LOOP_LEARNING) + "% \tW : " + neurons[0].synapses[0] + " \tB : " + neurons[0].synapses[1]);
		}
		
		//3) Calculate weight's & biases averages
		float weightAvg = neurons[0].synapses[0]; //0.0f;
		float biasAvg = neurons[0].synapses[1]; //0.0f;
		/*for(int nbWeight=0; nbWeight<NB_LOOP_LEARNING; nbWeight++) { //For each weight (1000000)
			weightAvg += weights.get(nbWeight);
			biasAvg += biases.get(nbWeight);
		}
		weightAvg /= NB_LOOP_LEARNING;
		biasAvg /= NB_LOOP_LEARNING;*/
		
		//4) Set weights parameters
		weightsGot.add(weightAvg);
		weightsGot.add(biasAvg);
				
		System.out.println("\nWEIGHT AVG : " + weightAvg + " \tBIAS AVG : " + biasAvg + '\n');
	}

	
	
	//Learn to the neuron's from inputs gets
	public void learning(float currentStiffnessValue, float resultWanted) {
		for(int n=0; n < this.neurons.length; n++){	//For each neurons
			this.neurons[n].learning(currentStiffnessValue, resultWanted);
		}
	}
	
	

	//SETTER & GETTER//
	public Neuron[] getNeurons() {
		return neurons;
	}
	public void setNeurons(Neuron[] neurons) {
		this.neurons = neurons;
	}
	
	
	
	//KEY LISTENER//
	public void keyPressed(KeyEvent evt){
		this.key = evt.getKeyChar();
	}
	public void keyReleased(KeyEvent evt){
		this.key = 'n';
	} 
 	public void keyTyped(KeyEvent evt) {}
}

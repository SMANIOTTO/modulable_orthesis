package fr.yncrea.modulableOrthesis.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Tools {

	//Save weight in weight_save.txt
	public void saveWeight(File weightFile, float weightToSave, float bias) {		
		try {
			
			//1) Create file if not existing
			if (weightFile.createNewFile()) {
				System.out.println("FILE -> weight_save.txt not found : created");
			}
			
			//2)  Write weight in
			FileWriter myWriter = new FileWriter("weight_save.txt");
			myWriter.write(Float.toString(weightToSave));
			myWriter.write('\n' + Float.toString(bias));
			myWriter.close();
			System.out.println("FILE -> Weight writed in file weight_save.txt");
		      
		} catch (IOException e) {
			System.out.println("ERROR : FILE -> File creation & Edition");
			e.printStackTrace();
		}
	}
	
	//Get weight from weight_save.txt
	public List<Float> getWeight(File weightFile) {
		List<Float> weightsGot = new ArrayList<Float>();
		
		try {
			
			//1) Open file
			Scanner reader = new Scanner(weightFile);
			
			//2) Get data from it
			while (reader.hasNextLine()) {
				String data = reader.nextLine();
				weightsGot.add(Float.parseFloat(data));
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("ERROR : FILE -> Unable to open file weight_save.txt");
		}
		
		return weightsGot;
	}
	
	
	
	//Display the different options
	public void menuDisplay() {
		System.out.println("///////////////////////////////// MAIN MENU /////////////////////////////////");
		System.out.println("(S) - Calibrate AI (start learning according to random stiffness input values)");
		System.out.println("(D) - Start AI decision (when launched, press 'SPACE' to generate stiffness)");
		System.out.println("(X) - Quit");
		System.out.println("/////////////////////////////////////////////////////////////////////////////");
	}
		
}

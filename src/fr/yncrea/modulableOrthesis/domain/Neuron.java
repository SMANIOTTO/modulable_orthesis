package fr.yncrea.modulableOrthesis.domain;

import java.text.DecimalFormat;

public class Neuron
{
	private float 	state 		= 0.15f;	//State
	private float 	threshold 	= 1.e-2f;	//Threshold
	public 	float[] synapses;				//weights & bias(last)
	public  float 	output;
	

	
	//CONSTRUCTOR//
	public Neuron(final int nbEntrees){
		
		//1) Add bias (last entry+1)
		this.synapses = new float[nbEntrees+1];
		
		//2) Set random weights & bias
		for(int i = 0; i < nbEntrees+1; ++i) {
			this.synapses[i] = (float)(Math.random()*2.-1.);
		}
	}
	

	
	//Function defining if threshold pass or not
	public float transferFunction(final float valeur) {
		return valeur >= 0 ? 1.f : 0.f;
	}



	//Calculate outputs according to weights, bias and transfer function
	public void update(float currentStiffnessValue){
		
		//1) Get bias
		float somme = this.synapses[this.synapses.length-1];
		
		//2) Calculate weighted sum
		for (int i = 0; i < this.synapses.length-1; ++i) {
			somme += currentStiffnessValue*this.synapses[i];
		}
		
		//3) Get correct neuron or not
		output = transferFunction(somme);
	}



	//Update weights & bias according to data wanted
	public void learning(float entry, float resultWanted) {
		boolean learningDone = false;

		while(!learningDone) {	
			learningDone = isSame(entry, resultWanted);

			if(!learningDone) {
				learn(entry, resultWanted);
				
				/*DecimalFormat formatter = new DecimalFormat("#.0");
				System.out.println("NO -> INPUT : " + formatter.format(entry) + "\t WEIGHTS : " +
						this.synapses[0] + "\t BIAS : " + this.synapses[this.synapses.length-1] + 
						"\t GOT : " + this.output + "\t WANTED : " + resultWanted);*/
				break;
			}
		}
		
		/*DecimalFormat formatter = new DecimalFormat("#.0");
		System.out.println("OK -> INPUT : " + formatter.format(entry) + "\t WEIGHTS : " +
		this.synapses[0] + "\t BIAS : " + this.synapses[this.synapses.length-1] + 
		"\t GOT : " + this.output + "\t WANTED : " + resultWanted);*/
	}
	//Check if input same as output
	public boolean isSame(float entry, float resultWanted) {
		//1) Get output
		update(entry);
		
		//2) test if the same
		return (output == resultWanted);
	}
	//Learn neurons's weights and bias from input and results wanted
	private void learn(float entry, float resultWanted) {
		
		//1) Calculate new weights
		for(int j=0; j < getSynapses().length-1; j++) {	//For each weights
			this.synapses[j] += entry * this.state * (resultWanted - this.output);//Weight formula
		}
		
		//2) Calculate new bias
		this.synapses[this.synapses.length-1] += this.state * (resultWanted - this.output);//Bias formula
	}



	//SETTER & GETTER//
	public float[] getSynapses() {
		return synapses;
	}

	public double getOutput() {
		return output;
	}
}
